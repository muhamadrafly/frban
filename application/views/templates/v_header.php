<!DOCTYPE html>
<html>

<head>

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <title>FRBan | <?= ucfirst($this->uri->segment(1)) ?></title>

   <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

   <link href="<?= base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
   <link href="<?= base_url() ?>assets/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
   <link href="<?= base_url() ?>assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

   <link href="<?= base_url() ?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

   <link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
   <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
   <style>
      @import url('https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

      * {
         font-family: 'Rubik', sans-serif;
      }
   </style>
</head>

<body>
   <div id="wrapper">

      <nav class="navbar-default navbar-static-side" role="navigation">
         <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
               <li class="nav-header">
                  <div class="dropdown profile-element">
                     <img alt="image" class="rounded-circle" src="<?= base_url() ?>assets/img/profile_small.jpg" />
                     <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold"><?= $this->session->userdata('full_name') ?></span>
                        <span class="text-muted text-xs block"><?= $this->session->userdata('role_name') ?> <b class="caret"></b></span>
                     </a>
                     <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="profile.html">Edit Profile</a></li>
                     </ul>
                  </div>
                  <div class="logo-element">
                     FR
                  </div>
               </li>
               <li>
                  <a href="<?= base_url() ?>dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
               </li>
               <li>
                  <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master Data</span> <span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse">
                     <li class="products_active"><a href="<?= base_url() ?>products/lists">Barang</a></li>
                     <li class="supplier_active"><a href="<?= base_url() ?>suppliers/lists">Supplier</a></li>
                     <li class="reseller_active"><a href="<?= base_url() ?>resellers/lists">Reseller</a></li>
                     <li class="customer_active"><a href="<?= base_url() ?>customers/lists">Pelanggan</a></li>
                     <li class="nacessary_active"><a href="<?= base_url() ?>nacessary/lists">Kebutuhan</a></li>
                  </ul>
               </li>
               <li>
                  <a href="<?= base_url() ?>warehouses"><i class="fa fa-cube"></i> <span class="nav-label">Gudang</span></a>
               </li>
               <li>
                  <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Transaksi</span> <span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level collapse">
                     <li class="t_service_active"><a href="<?= base_url() ?>transactions/service">Jasa Service</a></li>
                     <li class="t_supplier_active"><a href="<?= base_url() ?>transactions/supplier">Supplier</a></li>
                     <li class="t_reseller_active"><a href="<?= base_url() ?>transactions/reseller">Reseller</a></li>
                     <li class="t_retur_active"><a href="<?= base_url() ?>transactions/retur">Retur</a></li>
                  </ul>
               </li>
               <li>
                  <a href="<?= base_url() ?>users"><i class="fa fa-user"></i> <span class="nav-label">Pengguna</span></a>
               </li>
               <li class="landing_link">
                  <a href="<?= base_url() ?>reports"><i class="fa fa-clipboard"></i> <span class="nav-label">Laporan</span> </a>
               </li>
               <li class="special_link">
                  <a href="<?= base_url() ?>settings"><i class="fa fa-gear"></i> <span class="nav-label">Setting</span> </a>
               </li>
            </ul>
         </div>
      </nav>

      <div id="page-wrapper" class="gray-bg">
         <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
               <div class="navbar-header">
                  <button type="button" class="navbar-minimalize minimalize-styl-2 btn btn-primary"><i class="fa fa-bars"></i></button>
                  <span class="nav minimalize-styl-2 mt-3" id="lifeTime"></span>
               </div>
               <ul class="nav navbar-top-links navbar-right">
                  <li>
                     <span class="m-r-sm text-muted welcome-message">Point of Sales <b>Toko FR Ban Cianjur</b></span>
                  </li>
                  <li>|</li>
                  <li>
                     <a href="<?= base_url() ?>auth/logout">
                        <i class="fa fa-sign-out"></i> Log out
                     </a>
                  </li>
               </ul>
            </nav>
         </div>