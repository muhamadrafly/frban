 <div class="footer">
    <div class="float-right">
       &copy; 2020-<?= date('Y') ?>
    </div>
    <div>
       <strong>Copyright</strong> Toko FR Ban Cianjur &bullet; Supported by <span class="text-success">Aditya M. Putra & M. R. El Quraish</span>
    </div>
 </div>

 </div>
 </div>

 <!-- Mainly scripts -->
 <script src="<?= base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
 <script src="<?= base_url() ?>assets/js/popper.min.js"></script>
 <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
 <script src="<?= base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
 <script src="<?= base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

 <script src="<?= base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
 <script src="<?= base_url() ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
 <script src="<?= base_url() ?>assets/js/jquery.form-validator.min.js"></script>

 <!-- Custom and plugin javascript -->
 <script src="<?= base_url() ?>assets/js/inspinia.js"></script>
 <script src="<?= base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
 <script>
    $(document).ready(function() {
       setInterval(lifeTime, 1000);

       function lifeTime() {
          let options = {
             year: 'numeric',
             month: 'long',
             day: 'numeric'
          };
          let today = new Date;
          let date = today.toLocaleTimeString('id-ID', options).split(' ');
          let second = date[3].split('.');
          $('#lifeTime').text(date[0] + ', ' + date[1] + ' ' + date[2] + ' - ' + second[0] + ':' + second[1] + ':' + second[2]);
       }
    })
 </script>


 </body>

 </html>