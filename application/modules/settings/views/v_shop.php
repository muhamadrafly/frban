<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Pengaturan Toko</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <span>Setting</span>
         </li>
         <li class="breadcrumb-item active">
            <strong>Pengaturan Toko</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><i class="fa fa-gears mr-2"></i>Informasi</h5>
            </div>
            <div class="ibox-content">
               <form method="get">
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Nama Toko</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" autocomplete="off" value="FR Ban Cianjur">
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Alamat</label>
                     <div class="col-sm-10">
                        <textarea class="form-control" autocomplete="off">Dk. Basmol Raya No. 435, Bau-Bau 47410, DKI</textarea>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Nomor Telephone</label>
                     <div class="col-sm-10">
                        <div class="input-group">
                           <input type="text" class="form-control" autocomplete="off" value="(+02) 893 1491 1230">
                        </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Logo</label>
                     <div class="col-sm-10">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                           <span class="btn btn-default btn-file"><span class="fileinput-new">Pilih foto</span>
                              <span class="fileinput-exists">Ubah</span><input type="file" name="...">
                           </span>
                           <span class="fileinput-filename"></span>
                           <a href="javascript:void(0)" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                        </div><br>

                        <img src="https://cl-pos.asika.id/assets/uploads/logo1.png" width="120" alt="">

                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-primary btn-sm" type="submit">Simpan Perubahan</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><i class="fa fa-gears mr-2"></i>Barang & Transaksi</h5>
            </div>
            <div class="ibox-content">
               <form method="get">
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Awalan Barcode</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" autocomplete="off" value="FRB">
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Awalan Nota</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" autocomplete="off" value="INVFR">
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group row">
                     <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-primary btn-sm" type="submit">Simpan Perubahan</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>
<!-- Jasny -->
<script src="<?= base_url() ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script>
   $(document).ready(function() {
      $('.fa-gear').parents().addClass('active')

      $('.custom-file-input').on('change', function() {
         let fileName = $(this).val().split('\\').pop();
         $(this).next('.custom-file-label').addClass("selected").html(fileName);
      });

   });
</script>