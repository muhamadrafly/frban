<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Kebutuhan</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <a>Master Data</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Kebutuhan</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah Kebutuhan</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-example">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <th>Tanggal</th>
                           <th>Jumlah(Rp)</th>
                           <th>Keterangan</th>
                           <th class="text-right" width="1px">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="gradeX">
                           <td>1</td>
                           <td>2021-09-15 20:32:11</td>
                           <td>200.000,-</td>
                           <td>Token Listrik</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>2021-09-12 20:01:01</td>
                           <td>245.500,-</td>
                           <td>Air Pam</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.nacessary_active').addClass('active')
      $('.fa-database').parents().addClass('active')
      $('.fa-database').parent().next().addClass('in')

      $('.dataTables-example').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
      });
   });
</script>