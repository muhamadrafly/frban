<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// parent::__session_check();

		$this->load->model('M_Sistem');
		$this->load->model('m_auth');
		$this->load->helper('login');
	}

	public function login()
	{
		$this->load->view('v_login');
	}

	public function process()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$query = $this->M_Sistem->_validateLogin($username, $password);

		if ($query) {
			$this->db->trans_start();

			$logUsers = array();
			$logUsers['username'] = $query['username'];
			$logUsers['password'] = $query['password'];
			$logUsers['last_login'] = date('Y-m-d H:i:s');
			$logUsers['ip_address'] = getClientIP();

			$this->M_Sistem->_update('users', $logUsers, array('username' => $query['username'], 'password' => $query['password']));

			$data = array();
			$data['users_id'] = $query['id'];
			$data['full_name'] = $query['full_name'];
			$data['username'] = $query['username'];
			$data['is_logged_in'] = true;

			$getUserRoles = $this->m_auth->getUserRoles($query['id']);

			$data['role_name'] = ucfirst($getUserRoles['name']);
			$data['redirect'] = base_url() . 'dashboard';
			$data['confirm'] = 'yes';
			$data['description'] = 'Login Berhasil';

			$this->session->set_userdata($data);
			$this->db->trans_complete();
		} else {
			$data['confirm'] = 'no';
			$data['description'] = 'Username atau Password salah!';
		}
		echo json_encode($data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth/login');
	}
}
