<?php

class M_Auth extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getUserRoles($users_id)
    {
        $this->db->select('u.*, r.*');
        $this->db->from('users u');
        $this->db->join('roles r', 'u.roles_id = r.id');
        $this->db->where('u.id', $users_id);

        $query = $this->db->get();

        return $query->row_array();
    }
}
