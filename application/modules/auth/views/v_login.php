<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="TOKO FR BAN CIANJUR">
  <meta name="keywords" content="TOKO FR BAN CIANJUR">
  <meta name="author" content="TOKO FR BAN CIANJUR">
  <link rel="icon" href="<?= base_url() ?>assets/auth/images/favicon.png" type="image/x-icon">
  <link rel="shortcut icon" href="<?= base_url() ?>assets/auth/images/favicon.png" type="image/x-icon">
  <title>TOKO FR BAN CIANJUR</title>
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/vendors/icofont.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/vendors/themify.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/vendors/flag-icon.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/vendors/feather-icon.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/vendors/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/style.css">
  <link id="color" rel="stylesheet" href="<?= base_url() ?>assets/auth/css/color-1.css" media="screen">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/auth/css/responsive.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/auth/css/vendors/sweetalert2.css">

  <style>
    /* Custom style baru */
    h3,
    span,
    .link {
      color: rgb(26, 179, 148) !important;
    }

    .btn-primary {
      background-color: rgb(26, 179, 148) !important;
      border: 1px solid rgb(26, 179, 148) !important;
    }

    #bg-image {
      background: url('http://www.tractionnews.com/wp-content/uploads/2017/08/tire-industry-research-e1503331426568.jpg') no-repeat;
      background-size: cover;
      position: relative;
    }

    #bg-image-after {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(26, 179, 148, 0.7);
      opacity: 6;
    }
  </style>

</head>

<body>
  <!-- login page start-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-7 order-1" id="bg-image">
        <div id="bg-image-after"></div>
      </div>
      <div class="col-xl-5 p-0">
        <div class="login-card">
          <div>
            <div>
              <a class="logo text-start text-center" href="javascript:void(0)">
                <h3>TOKO FR BAN CIANJUR</h3>
              </a>
            </div>
            <div class="login-main">


              <form class="theme-form needs-validation" novalidate="" method="POST" id="formLogin">
                <div class="form-group">
                  <label class="col-form-label">Username</label>
                  <input class="form-control" type="text" name="username" required="" autofocus tabindex="1">
                  <div class="invalid-feedback text-danger">Username tidak boleh kosong.</div>
                </div>
                <div class="form-group">
                  <label class="col-form-label">Password</label>
                  <div class="form-input position-relative">
                    <input class="form-control" type="password" name="password" id="password" required="" placeholder="*********" tabindex="2">
                    <div class="show-hide"><a href="javascript:void(0)" class="text-success show out"><i class="fa fa-eye"></i></a></div>
                    <div class="invalid-feedback text-danger">Password tidak boleh kosong.</div>
                  </div>
                </div>
                <div class="form-group mb-0">
                  <div class="checkbox p-0">
                    <input id="checkbox1" type="checkbox">
                    <label class="text-muted" for="checkbox1">Ingat saya?</label>
                  </div><a class="link" href="#">Lupa password?</a>
                  <div class="text-end mt-3">
                    <button class="btn btn-primary btn-block w-100" type="submit" id="submit" tabindex="3">Log In</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url() ?>assets/auth/js/jquery-3.5.1.min.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/icons/feather-icon/feather-icon.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/config.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/script.js"></script>
    <script src="<?= base_url(); ?>assets/auth/js/sweet-alert/sweetalert.min.js"></script>

    <script>
      $(function() {
        'use strict';
        window.addEventListener('load', function() {
          let forms = document.getElementsByClassName('needs-validation');
          let validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
              $(".show").hide()
            }, false);
          });
        }, false);

        $(".show").click(function(e) {
          if ($(this).hasClass('out')) {
            $('#password').attr('type', 'text')
            $(this).removeClass('out').addClass('in').html('<i class="fa fa-eye-slash"></i>')
          } else {
            $('#password').attr('type', 'password')
            $(this).removeClass('in').addClass('out').html('<i class="fa fa-eye"></i>')
          }
        });

        $(document).on('submit', '#formLogin', function(e) {
          e.preventDefault()

          $('body').append(`
            <div class="loader-wrapper">
              <div class="loader-index"><span></span></div>
              <svg>
                <defs></defs>
                <filter id="goo">
                  <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
                  <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"> </fecolormatrix>
                </filter>
              </svg>
            </div>
            `);

          setTimeout(login, 100);

          function login() {
            $('.loader-wrapper').fadeOut('slow', function() {
              $(this).remove()
            })

            $.ajax({
              url: '<?= base_url(); ?>auth/process',
              type: 'POST',
              data: $('#formLogin').serialize(),
              cache: false,
              success: function(data) {
                let result = JSON.parse(data)
                if (result.confirm == 'yes') {
                  window.location.href = result.redirect
                } else {
                  swal({
                    title: result.description,
                    icon: 'warning',
                    showConfirmButton: true
                  }).then(function() {
                    $('#formLogin').trigger('reset')
                    $('input[name="username"]').focus()
                  })
                }
              }
            })

          }
        })
      })
    </script>

  </div>
</body>

</html>