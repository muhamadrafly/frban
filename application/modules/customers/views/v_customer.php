<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Pelanggan</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <a>Master Data</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Pelanggan</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah Pelanggan</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-example">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <th>Nama</th>
                           <!-- <th>Email</th> -->
                           <th>Telephone</th>
                           <th>Alamat</th>
                           <th width="1px">Status</th>
                           <th class="text-right" width="1px">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="gradeX">
                           <td>1</td>
                           <td>Aditya M. Gaming</td>
                           <!-- <td>Aditya@gmail.com</td> -->
                           <td>085315823310</td>
                           <td class="center"><?= substr('Lur ipsum dolor, sit amet consectetur adipisicing elit. Ut, ducimus!', 0, 50) ?></td>
                           <td class="center"><span class="label label-success">Active</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>Muhammad Rafly El Quraish</td>
                           <!-- <td>Quraish@gmail.com</td> -->
                           <td>085315823311</td>
                           <td class="center"><?= substr('Pler ipsum dolor sit amet.', 0, 50) ?></td>
                           <td class="center"><span class="label label-danger">Non Active</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>Maman Galon</td>
                           <!-- <td>Galon@gmail.com</td> -->
                           <td>085315823312</td>
                           <td class="center"><?= substr('Coek ipsum dolor sit amet consectetur, adipisicing elit. Enim voluptatibus inventore dolores excepturi provident animi aliquam deleniti adipisci minus debitis!', 0, 50) ?></td>
                           <td class="center"><span class="label label-success">Active</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.customer_active').addClass('active')
      $('.fa-database').parents().addClass('active')
      $('.fa-database').parent().next().addClass('in')

      $('.dataTables-example').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
      });
   });
</script>