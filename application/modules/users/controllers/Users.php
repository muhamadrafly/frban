<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      parent::__session_check();

      $this->load->model('M_user');
   }

   public function index()
   {
      $this->load->view('v_user');
   }

   function getUserServerSide()
   {
      $list = $this->M_user->_getDatatables();
      $data = array();
      $no = $_POST['start'];
      $draw = $_POST['draw'];

      foreach ($list as $value) {
         $no++;
         $row = array();
         $row[] = $no;
         $row[] = $value->full_name;
         $row[] = $value->username;
         $row[] = $value->phone;
         $row[] = ucfirst($value->role_name);
         $data[] = $row;
      }

      $output = [
         'draw' => $draw,
         'recordsTotal' => $this->M_user->_countAll(),
         'recordsFiltered' => $this->M_user->_countFiltered(),
         'data' => $data
      ];

      echo json_encode($output);
   }
}
