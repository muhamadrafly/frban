<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Pengguna</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Pengguna</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalAddEdit"><i class="fa fa-plus-square mr-1"></i> Tambah Pengguna [F2]</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTable" width="100%">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <!-- <th width="1px">Foto</th> -->
                           <th>Nama Lengkap</th>
                           <th>Username</th>
                           <th>Telephone</th>
                           <th>Posisi</th>
                           <!-- <th class="text-right" width="1px">Action</th> -->
                        </tr>
                     </thead>
                     <tbody>

                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="ModalAddEdit" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <form action="#" id="formAddEdit" method="POST">
            <div class="modal-header">
               <h4 class="modal-title">Tambah Pengguna</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="name" name="name" autocomplete="off" tabindex="1" required maxlength="50">
                           <small class="text-danger" id="name_error"></small>
                        </div>
                     </div>

                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="username" name="username" autocomplete="off" tabindex="2" required minlength="5" maxlength="30">
                           <small class="text-danger" id="username_error"></small>
                        </div>
                     </div>

                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                           <input type="password" class="form-control" id="password" name="password" autocomplete="off" tabindex="3" required minlength="6" maxlength="30">
                           <small class="text-danger" id="password_error"></small>
                           <div class="checkbox pt-1">
                              <input type="checkbox" id="hide_show"> Tampilkan password?
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-6">
                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Telephone</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" autocomplete="off" tabindex="4" maxlength="20">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Role</label>
                        <div class="col-sm-9">
                           <select class="form-control chosen-select" id="roles_id" name="roles_id" tabindex="5" required>
                              <option value="" selected disabled>Pilih Role...</option>
                              <option value="1">Admin</option>
                              <option value="2">Operator</option>
                           </select>
                           <small class="text-danger" id="roles_id_error"></small>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
            <div class="modal-footer justify-content-between" style="margin-top: -15px;">
               <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-rectangle-o mr-1"></i>Tutup [Esc]</button>
               <button type="submit" class="btn btn-primary" id="submit" tabindex="6"><i class="fa fa-check-square mr-1"></i>Simpan [Enter]</button>
            </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>

<?php $this->load->view('templates/v_footer'); ?>
<!-- Chosen -->
<script src="<?= base_url() ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<script>
   $(document).ready(function() {
      $('.fa-user').parents().addClass('active')

      document.onkeydown = function(e) {
         (e.keyCode == 113) ? $('#ModalAddEdit').modal('show'): false;
      }

      $('.dataTable').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
         ajax: {
            url: '<?= base_url() ?>users/getUserServerSide',
            type: "POST",
         },
         columnDefs: [{
            'targets': [0],
            'orderable': false,
         }],
      });

      $('.chosen-select').chosen({
         width: "100%",
      });

      $(document).on('click change', '#hide_show', function() {
         $(this).prop('checked') === true ? $('#password').attr('type', 'text'): $('#password').attr('type', 'password')
      })

      $('#ModalAddEdit').on('shown.bs.modal', function(e) {
         $('#name').focus();
         let button = e.relatedTarget;
      })

      $("#formAddEdit").validate({
         messages: {
            name: "Nama tidak boleh kosong",
            username: {
               required: "Username tidak boleh kosong",
               minlength: "Username minimal 5 karakter"
            },
            password: {
               required: "Password tidak boleh kosong",
               minlength: "Password minimal 6 karakter"
            },
            roles_id: "Role tidak boleh kosong",
         },
         success: function(messages) {
            $(messages).remove();
         },
         errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").text(error.text());
         },
         submitHandler: function(form) {
            alert('success')
         }
      });

   });
</script>