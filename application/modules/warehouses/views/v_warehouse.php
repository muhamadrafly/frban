<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Gudang</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Gudang</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah Gudang</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-example">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <th>Nama</th>
                           <th>Kapasitas</th>
                           <th width="1px">Status</th>
                           <th class="text-right" width="1px">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="gradeX">
                           <td>1</td>
                           <td>Gudang Atas</td>
                           <td>50 kuantitas</td>
                           <td class="center"><span class="label label-success">Belum Penuh</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>Gudang Bawah</td>
                           <td>250 kuantitas</td>
                           <td class="center"><span class="label label-success">Belum Penuh</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>3</td>
                           <td>Gudang Belakang</td>
                           <td>500 kuantitas</td>
                           <td class="center"><span class="label label-danger">Penuh</span></td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.fa-cube').parents().addClass('active')

      $('.dataTables-example').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
      });
   });
</script>