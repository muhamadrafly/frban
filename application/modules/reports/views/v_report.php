<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>List Laporan</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Laporan</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-3">
         <a href="">
            <div class="widget style1 navy-bg">
               <div class="row">
                  <div class="col-2">
                     <i class="fa fa-shopping-cart fa-5x"></i>
                  </div>
                  <div class="col-10 text-right">
                     <span>Laporan Penjualan</span>
                     <h2 class="font-bold"><i class="fa fa-arrow-circle-o-right"></i></h2>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-lg-3">
         <a href="">
            <div class="widget style1 lazur-bg">
               <div class="row">
                  <div class="col-2">
                     <i class="fa fa-money fa-5x"></i>
                  </div>
                  <div class="col-10 text-right">
                     <span>Laporan Keuntungan</span>
                     <h2 class="font-bold"><i class="fa fa-arrow-circle-o-right"></i></h2>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.fa-clipboard').parents().addClass('active')
   })
</script>