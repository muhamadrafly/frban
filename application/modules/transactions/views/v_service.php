<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Jasa Service</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <a>Transaksi</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Jasa Service</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-content">

               <div class="row">
                  <div class="col-md-9">
                     <div class="form-group row">
                        <label for="" class="col-sm-1 col-form-label">Barcode</label>
                        <div class="col-sm-3">
                           <div class="input-group">
                              <input type="text" class="form-control" id="barcode" autocomplete="off">
                              <span class="input-group-append">
                                 <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
                              </span>
                           </div>
                        </div>
                        <label for="" class="col-sm-8 col-form-label" style="padding: 5px;"></label>
                     </div>

                     <style>
                        input::-webkit-outer-spin-button,
                        input::-webkit-inner-spin-button {
                           -webkit-appearance: none;
                           margin: 0;
                        }
                     </style>

                     <div class="form-group row">
                        <label for="" class="col-sm-1 col-form-label">Quantity</label>
                        <div class="col-sm-2">
                           <input class="touchspin3 form-control" type="number" autocomplete="off" placeholder="0" min="1" value="1">
                        </div>
                     </div>

                     <div class="btn-transactions">
                        <button tabindex="3" class="btn btn-primary btn-sm" type="button" disabled><i class="fa fa-plus-square mr-1"></i>Tambah</button>&nbsp;
                        <button tabindex="4" class="btn btn-primary btn-sm" type="button" disabled><i class="fa fa-credit-card mr-1"></i>Bayar</button>&nbsp;
                        <button tabindex="5" class="btn btn-info btn-sm" type="button" onclick="location.reload()" disabled><i class="fa fa-shopping-cart mr-1"></i>Transaksi Baru</button>&nbsp;
                        <button tabindex="6" class="btn btn-warning btn-sm" type="button" disabled><i class="fa fa-hand-paper-o mr-1"></i>Hold</button>&nbsp;
                        <button tabindex="7" class="btn btn-default btn-sm" type="button"><i class="fa fa-tasks mr-1"></i>Hold List</button>&nbsp;
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div style="display: flex; justify-content: end;">
                        <label for="" style="padding-top: 6px;">Nota :</label>
                        <input type="text" class="text-bold text-right" readonly disabled style="width: 150px; font-size: 17px; background-color: rgba(0,0,0,0) !important; border: 0 !important;" id="invoice" name="invoice" value="INVFR<?= date('md') ?>00001">
                     </div>
                     <p style="font-size: 45px; display: flex; justify-content: end;"><b>Rp. </b><b>0</b></p>
                  </div>
               </div>

               <table class="table table-bordered" style="margin-top: 2em;">
                  <thead class="text-center">
                     <tr>
                        <th width="1px" class="text-center">No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Quantity</th>
                        <th>Diskon</th>
                        <th>Total</th>
                        <th width='1px' class="text-center">Aksi</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr class="text-center">
                        <td colspan="7">Tidak ada item produk!</td>
                     </tr>
                  </tbody>
               </table>

            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>
<script src="<?= base_url() ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script>
   $(document).ready(function() {
      $('.t_service_active').addClass('active')
      $('.fa-shopping-cart').parents().addClass('active')
      $('.fa-shopping-cart').parent().next().addClass('in')

      $(".touchspin3").TouchSpin({
         verticalbuttons: true,
         buttondown_class: 'btn btn-white',
         buttonup_class: 'btn btn-white'
      });
   });
</script>