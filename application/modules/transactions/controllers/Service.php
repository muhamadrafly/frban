<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Service extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      parent::__session_check();
   }

   public function index()
   {
      $this->load->view('v_service');
   }
}
