<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Supplier</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <a>Master Data</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Supplier</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah Supplier</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-example">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <th>Kode</th>
                           <th>Nama</th>
                           <th>Telephone</th>
                           <th>Alamat</th>
                           <th>Kota</th>
                           <th>Provinsi</th>
                           <th class="text-right" width="1px">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="gradeX">
                           <td>1</td>
                           <td>SP0001</td>
                           <td>PT. Mansur</td>
                           <td>+02 1244 5148 1</td>
                           <td>Jl. Karangtengah no.19 RT/RW 02/09, Bojong</td>
                           <td>Cianjur</td>
                           <td>Jawa Barat</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>SP0002</td>
                           <td>PT. Sejahtera</td>
                           <td>+82 091284 01284</td>
                           <td>Jl. Abimanyu no.1 RT/RW 04/07, Bojong</td>
                           <td>Kebon Jeruk</td>
                           <td>DKI Jakarta</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>3</td>
                           <td>SP0003</td>
                           <td>PT. Makmur</td>
                           <td>+02 192781 8127</td>
                           <td>Jl. Nakula no.4 RT/RW 09/14, Karang Anyar</td>
                           <td>Cianjur</td>
                           <td>Jawa Barat</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.supplier_active').addClass('active')
      $('.fa-database').parents().addClass('active')
      $('.fa-database').parent().next().addClass('in')

      $('.dataTables-example').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
         // aoColumnDefs: [{
         //    "orderable": false,
         //    "aTargets": [0]
         // }, ]
      });
   });
</script>