<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		parent::__session_check();
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view('v_dashboard');
	}
}
