<?php $this->load->view('templates/v_header'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Data Barang</h2>
      <ol class="breadcrumb">
         <li class="breadcrumb-item">
            <a href="<?= base_url() ?>dashboard">Home</a>
         </li>
         <li class="breadcrumb-item">
            <a>Master Data</a>
         </li>
         <li class="breadcrumb-item active">
            <strong>Barang</strong>
         </li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox ">
            <div class="ibox-title">
               <h5><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#incomeModal"><i class="fa fa-plus-square mr-1"></i>Tambah Barang</button></h5>
            </div>
            <div class="ibox-content">

               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-example">
                     <thead>
                        <tr>
                           <th width="1px">#</th>
                           <th>Nama</th>
                           <th>Merk</th>
                           <th>Jenis</th>
                           <th class="text-right" width="1px">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="gradeX">
                           <td>1</td>
                           <td>Ban Sepeda</td>
                           <td>Dunlop</td>
                           <td>Radial</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>2</td>
                           <td>Ban Sepeda</td>
                           <td>Kingston</td>
                           <td>Tubles</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                        <tr class="gradeX">
                           <td>3</td>
                           <td>Ban Sepeda</td>
                           <td>Acheles</td>
                           <td>Bias</td>
                           <td class="text-right">
                              <div class="btn-group">
                                 <button class="btn-white btn btn-xs">Detail</button>
                                 <button class="btn-white btn btn-xs">Edit</button>
                                 <button class="btn-white btn btn-xs">Hapus</button>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="incomeModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="<?= base_url() ?>transactions/income/store" id="incomeForm" method="POST">
            <div class="modal-header">
               <h4 class="modal-title">Tambah Barang</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">

               <div class="form-group row has-error">
                  <label for="" class="col-sm-2 col-form-label">Nama <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" autocomplete="off">
                     <small class="text-danger">Nama tidak boleh kosong.</small>
                  </div>
               </div>

               <div class="form-group row has-error">
                  <label for="" class="col-sm-2 col-form-label">Merk <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" autocomplete="off">
                     <small class="text-danger">Merk tidak boleh kosong.</small>
                  </div>
               </div>

               <div class="form-group row has-error">
                  <label for="" class="col-sm-2 col-form-label">Jenis <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" autocomplete="off">
                     <small class="text-danger">Jenis tidak boleh kosong.</small>
                  </div>
               </div>

            </div>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-default " data-dismiss="modal"><i class="fa fa-times-circle mr-1"></i>Tutup</button>
               <button type="button" class="btn btn-primary" id="submitIncome"><i class="fa fa-check-square mr-1"></i>Simpan</button>
            </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">

         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title">Modal title</h2>
         </div>
         <div class="modal-body">

         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
         </div>
      </div>
   </div>
</div>


<div id="modal-form" class="modal fade" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
               <span aria-hidden="true">&times;</span>
               <span class="sr-only">Close</span>
            </button>
            <i class="fa fa-laptop modal-icon"></i>
            <h4 class="modal-title">Modal title</h4>
            <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
         </div>
         <div class="modal-body">
            <!-- <div class="row">
               <div class="col-sm-6 pull-left">
                  <h3 class="m-t-none m-b">Tambah Barang</h3>
               </div>
               <div class="col-sm-6 pull-right">

               </div>
            </div> -->
            <!-- <div class="row">
               <div class="col-sm-6 b-r">
                  <h3 class="m-t-none m-b">Sign in</h3>
                  <hr>
                  <p>Sign in today for more expirience.</p>

                  <form role="form">
                     <div class="form-group"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control"></div>
                     <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div>
                     <div>
                        <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><strong>Log in</strong></button>
                        <label> <input type="checkbox" class="i-checks"> Remember me </label>
                     </div>
                  </form>
               </div>
               <div class="col-sm-6">
                  <h4>Not a member?</h4>
                  <p>You can create an account:</p>
                  <p class="text-center">
                     <a href=""><i class="fa fa-sign-in big-icon"></i></a>
                  </p>
               </div>
            </div> -->
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('templates/v_footer'); ?>

<script>
   $(document).ready(function() {
      $('.products_active').addClass('active')
      $('.fa-database').parents().addClass('active')
      $('.fa-database').parent().next().addClass('in')

      $('.dataTables-example').DataTable({
         pageLength: 25,
         responsive: true,
         serverside: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [],
         // aoColumnDefs: [{
         //    "orderable": false,
         //    "aTargets": [0]
         // }, ]
      });
   });
</script>