<?php

defined('BASEPATH') or exit('No direct script access allowed');


class M_Sistem extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function _validateLogin($username, $password)
    {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('users.username', $username);
        $this->db->where('users.password', $password);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return [];
        }
    }

    public function _input($table, $data)
    {
        $query = $this->db->insert($table, $data);
        return $query;
    }

    public function _inputAll($table, $data)
    {
        $query = $this->db->insert_batch($table, $data);
        return $query;
    }

    public function _get($table, $orderBy = "")
    {
        if (!empty($orderBy)) {
            // Ex : field-ordername
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getSpesipic($table, $spesipic = "", $orderBy = "")
    {
        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $this->db->select($spesipic);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getSpesipicWhere($table, $spesipic = "", $orderBy = "", $where = "")
    {
        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $this->db->select($spesipic);
        $this->db->where($where);

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getSpesipicMaxWhere($table, $spesipic = "", $where = "", $max = "")
    {
        $this->db->select($spesipic);
        $this->db->select_max($max);
        $this->db->where($where);

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getWhereId($table, $id)
    {
        $this->db->where($id);

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return [];
        }
    }

    public function _getRowSpecific($table, $spesipic, $orderBy)
    {
        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $this->db->select($spesipic);

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return [];
        }
    }

    public function _getWhereNotIn($table, $where_field, $value, $orderBy = "")
    {

        $this->db->where_not_in($where_field, $value);

        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getWheres($table, $where, $orderBy = "")
    {
        $this->db->where($where);

        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getAll($table, $orderBy = "")
    {
        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function _getLimit($table, $orderBy, $limit, $offset)
    {
        if (!empty($orderBy)) {
            $order = explode("-", $orderBy);
            $this->db->order_by($order['0'], $order[1]);
        }

        $this->db->limit($offset, $limit);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    // public function _getWheresLimit($table, $orderBy = "", $where, $limit, $offset)
    // {
    //     $this->db->where($where);

    //     if (!empty($orderBy)) {
    //         $order = explode("-", $orderBy);
    //         $this->db->order_by($order['0'], $order[1]);
    //     }

    //     $this->db->limit($offset, $limit);
    //     $query = $this->db->get($table);

    //     if ($query->num_rows() > 0) {
    //         return $query->result_array();
    //     } else {
    //         return [];
    //     }
    // }

    public function _getWheresCount($table, $where)
    {
        $this->db->where($where);
        $this->db->get($table);

        $query = $this->db->count_all_results();
        return $query;
    }

    public function _delete($table, $where)
    {
        $query = $this->db->delete($table, $where);
        return $query;
    }

    public function _update($table, $data, $where)
    {
        $query = $this->db->update($table, $data, $where);
        return $query;
    }
}
